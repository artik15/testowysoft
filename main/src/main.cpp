#include <stdio.h>
#include "main.h"

#include "esp_log.h"

#define LOG_TAG "MAIN"
#define LOG_LEVEL_LOCAL ESP_LOG_VERBOSE

static main my_main;
//usunac

extern "C" void app_main(void)
{
    ESP_ERROR_CHECK(my_main.setup());

    while (true)
    {
        my_main.run();
    }
    
}

esp_err_t main::setup(void)
{
    esp_err_t status{ESP_OK};
    ESP_LOGI(LOG_TAG, "Setup!");

    return status;
}

void main::run(void)
{
    ESP_LOGI(LOG_TAG, "Hello World!");
    vTaskDelay(pdSECOND);
}